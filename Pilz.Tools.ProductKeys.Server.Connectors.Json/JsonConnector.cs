﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Pilz.Tools.ProductKeys.Server.Connectors.Json
{
    public class JsonConnector : DatabaseConnector
    {
        public JsonDatabase? Database { get; private set; }
        public string? DatabaseFilePath { get; private set; }
        public bool EnableAutoSave { get; set; }

        public void LoadDB(string filePath)
        {
            using var fs = File.OpenRead(filePath);
            using var streamReader = new StreamReader(fs);
            using var jsonReader = new JsonTextReader(streamReader);
            var serializer = JsonSerializer.CreateDefault();
            serializer.Converters.Add(new StringEnumConverter());
            serializer.Deserialize<JsonDatabase>(jsonReader);
            DatabaseFilePath = filePath;
        }

        public void SaveDB()
        {
            if (!string.IsNullOrWhiteSpace(DatabaseFilePath))
                SaveDB(DatabaseFilePath);
        }

        public void SaveDB(string filePath)
        {
            using var fs = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite);
            using var streamWriter = new StreamWriter(fs);
            var serializer = JsonSerializer.CreateDefault();
            serializer.Converters.Add(new StringEnumConverter());
            serializer.Formatting = Formatting.Indented;
            serializer.Serialize(streamWriter, Database);
        }

        private void AutoSave()
        {
            if (EnableAutoSave && Database != null)
                SaveDB();
        }

        public override ProductKey? Find(string key)
        {
            return Database?.Keys.FirstOrDefault(n => n.Key == key);
        }

        public override ProductKey? Find(long identicator)
        {
            return Database?.Keys.FirstOrDefault(n => n.Identicator == identicator);
        }

        public override bool Update(ProductKey productKey)
        {
            if (Database == null || !Database.Keys.Contains(productKey))
                return false;

            AutoSave();

            return true;
        }

        public override void Dispose()
        {
        }
    }
}