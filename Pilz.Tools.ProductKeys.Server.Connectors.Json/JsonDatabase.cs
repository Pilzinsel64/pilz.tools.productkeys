﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Server.Connectors.Json
{
    public class JsonDatabase
    {
        public List<ProductKey> Keys { get; } = new();
    }
}
