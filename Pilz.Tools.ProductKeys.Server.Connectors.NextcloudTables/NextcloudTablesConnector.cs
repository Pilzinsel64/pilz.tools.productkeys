﻿using Pilz.Networking.CloudProviders.Nextcloud;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model;

namespace Pilz.Tools.ProductKeys.Server.Connectors.NextcloudTables
{
    public class NextcloudTablesConnector : DatabaseConnector
    {
        private readonly NextcloudClient client;
        private readonly NextcloudTablesConnectorSettings settings;

        private TablesClient Tables => client.GetClient<TablesClient>();

        public NextcloudTablesConnector(NextcloudClient client, NextcloudTablesConnectorSettings settings)
        {
            this.client = client;
            this.settings = settings;
        }

        public override ProductKey? Find(string key)
        {
            if (Tables.GetRows(settings.TableId) is Rows rows)
            {
                foreach (Row row in rows)
                {
                    foreach (RowData data in row.Data)
                    {
                        if (data.ColumnId == settings.ColumnIdKey && data.Value is string value && value == key)
                            return GetProductKey(row);
                    }
                }
            }
            return null;
        }

        public override ProductKey? Find(long identicator)
        {
            if (Tables.GetRow(identicator) is Row row)
                return GetProductKey(row);
            return null;
        }

        private ProductKey GetProductKey(Row row)
        {
            string? key = null;
            ProductKeyState? state = null;
            string? clientSecret = null;
            DateTime? lastAccess = null;

            foreach (RowData data in row.Data)
            {
                if (data.Value != null)
                {
                    if (data.ColumnId == settings.ColumnIdKey)
                        key = data.Value as string;
                    else if (data.ColumnId == settings.ColumnIdState)
                        state = (ProductKeyState)(long)data.Value;
                    else if (data.ColumnId == settings.ColumnIdClientSecret)
                        clientSecret = data.Value as string;
                    else if (data.ColumnId == settings.ColumnIdLastAccess)
                        lastAccess = Convert.ToDateTime(data.Value);
                }
            }

            return new(key ?? string.Empty, row.RowId, state ?? ProductKeyState.Inactive, clientSecret, lastAccess);
        }

        public override bool Update(ProductKey productKey)
        {
            RowUpdate update = new();
            update.Data.Add(settings.ColumnIdClientSecret, productKey.ClientSecret);
            update.Data.Add(settings.ColumnIdLastAccess, productKey.LastAccess?.ToString());
            return Tables.UpdateRow(productKey.Identicator, update) != null;
        }

        public override void Dispose()
        {
            client.Dispose();
        }
    }
}