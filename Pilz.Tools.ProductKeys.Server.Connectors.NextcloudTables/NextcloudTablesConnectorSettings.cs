﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Server.Connectors.NextcloudTables
{
    public class NextcloudTablesConnectorSettings
    {
        public long TableId { get; set; }
        public long ColumnIdKey { get; set; }
        public long ColumnIdState { get; set; }
        public long ColumnIdClientSecret { get; set; }
        public long ColumnIdLastAccess { get; set; }
    }
}
