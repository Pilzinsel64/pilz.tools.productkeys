﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Server
{
    public abstract class DatabaseConnector : IDisposable
    {
        public abstract ProductKey? Find(string key);
        public abstract ProductKey? Find(long identicator);
        public abstract bool Update(ProductKey productKey);
        public abstract void Dispose();
    }
}
