﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Server
{
    internal class ProcessingRequestEventArgs : EventArgs
    {
        public string Path { get; set; }
        public string? InputContent { get; set; }
        public string? ResponseContent { get; set; } = null;
        public HttpStatusCode ResponseStatusCode { get; set; } = HttpStatusCode.BadRequest;
        public string ResponseStatusDescription { get; set; } = string.Empty;

        public ProcessingRequestEventArgs(string path, string? inputContent)
        {
            Path = path;
            InputContent = inputContent;
        }
    }
}
