﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Server
{
    public class ProductKey
    {
        public string Key { get; init; }
        public long Identicator { get; init; }
        public ProductKeyState State { get; init; }
        public string? ClientSecret { get; set; }
        public DateTime? LastAccess { get; set; }

        public ProductKey(string key, long identicator, ProductKeyState state)
        {
            Key = key;
            Identicator = identicator;
            State = state;
        }

        public ProductKey(string key, long identicator, ProductKeyState state, string? clientSecret, DateTime? lastAccess) : this(key, identicator, state)
        {
            ClientSecret = clientSecret;
            LastAccess = lastAccess;
        }
    }
}
