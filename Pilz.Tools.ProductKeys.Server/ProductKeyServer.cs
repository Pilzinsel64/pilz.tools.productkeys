﻿using Newtonsoft.Json;
using Pilz.Tools.ProductKeys.Requests;

namespace Pilz.Tools.ProductKeys.Server
{
    public class ProductKeyServer : IDisposable
    {
        private delegate object? ProcessRequestHandler(ProcessingRequestEventArgs e, object? inputContent);

        private readonly DatabaseConnector connector;
        private readonly HttpServer server;
        private readonly TextWriter output;

        public bool IsRunning { get; private set; } = false;

        public ProductKeyServer(string serverUrl, DatabaseConnector connector, TextWriter output)
        {
            this.output = output;
            this.connector = connector;
            server = new(serverUrl);
            server.ProcessingRequest += Server_ProcessingRequest;
        }

        private void Server_ProcessingRequest(object sender, ProcessingRequestEventArgs e)
        {
            Type? inputContentType = null;
            ProcessRequestHandler? processMethod = null;

            switch (e.Path)
            {
                case UrlRequestPaths.VALIDATE_NEW_KEY:
                    inputContentType = typeof(NewKeyValidationRequest);
                    processMethod = ProcessNewKeyRequest;
                    break;
                case UrlRequestPaths.VALIDATE_EXISTNG_KEY:
                    inputContentType = typeof(ExistingKeyValidationRequest);
                    processMethod = ProcessExistingKeyRequest;
                    break;
            }

            if (processMethod != null)
            {
                object? inputContent = null;

                if (inputContentType != null && e.InputContent != null)
                    inputContent = JsonConvert.DeserializeObject(e.InputContent, inputContentType);

                if (processMethod(e, inputContent) is object outputContent)
                    e.ResponseContent = JsonConvert.SerializeObject(outputContent);
            }
            else
                e.ResponseStatusCode = System.Net.HttpStatusCode.BadRequest;
        }

        private object? ProcessNewKeyRequest(ProcessingRequestEventArgs e, object? inputContent)
        {
            object? response = null;

            if (inputContent is NewKeyValidationRequest request)
            {
                ProductKey? key = connector.Find(request.ProductKey);

                if (key?.Identicator != null && key.State == ProductKeyState.Active)
                {
                    key.LastAccess = DateTime.Now;
                    key.ClientSecret = request.ClientSecret;

                    if (connector.Update(key))
                    {
                        response = new NewKeyValidationResponse
                        {
                            KeyIdentification = key.Identicator
                        };

                        e.ResponseStatusCode = System.Net.HttpStatusCode.OK;
                    }
                    else
                        e.ResponseStatusCode = System.Net.HttpStatusCode.InternalServerError;
                }
                else
                    e.ResponseStatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else
                e.ResponseStatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;

            return response;
        }

        private object? ProcessExistingKeyRequest(ProcessingRequestEventArgs e, object? inputContent)
        {
            if (inputContent is ExistingKeyValidationRequest request)
            {
                ProductKey? key = connector.Find(request.KeyIdentification);

                if (key?.ClientSecret == request.ClientSecret && key.State == ProductKeyState.Active)
                {
                    key.LastAccess = DateTime.Now;
                    connector.Update(key);
                    e.ResponseStatusCode = System.Net.HttpStatusCode.OK;
                }
                else
                    e.ResponseStatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
            else
                e.ResponseStatusCode = System.Net.HttpStatusCode.UnsupportedMediaType;

            return null;
        }

        public void StartServer()
        {
            server.Start();
        }
        
        public void StopServer()
        {
            server.Stop();
        }

        public void Dispose()
        {
            StopServer();
            connector.Dispose();
        }
    }
}