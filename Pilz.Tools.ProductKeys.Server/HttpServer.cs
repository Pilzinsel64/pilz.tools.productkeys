﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Server
{
    internal class HttpServer
    {
        public delegate void ProcessingRequestEventHandler(object sender, ProcessingRequestEventArgs e);
        public event ProcessingRequestEventHandler? ProcessingRequest;
     
        private readonly HttpListener listener = new();

        public string BaseUrl { get; init; }

        public HttpServer(string url)
        {
            BaseUrl = url;
            listener.Prefixes.Add(url);
        }

        public void Start()
        {
            listener.Start();
            Task.Run(Listen);
        }

        public void Stop()
        {
            listener.Stop();
        }

        private void Listen()
        {
            while (listener.IsListening)
            {
                HttpListenerContext context = listener.GetContext();

                if (context.Request.HttpMethod == HttpMethod.Get.Method &&
                    context.Request.ContentType is string contentType && contentType.Contains(ContentTypes.CONTENT_TYPE_JSON) &&
                    context.Request.AcceptTypes is not null &&
                    context.Request.AcceptTypes.Contains(ContentTypes.CONTENT_TYPE_JSON))
                {
                    string? path = context.Request.Url?.PathAndQuery.ToString();

                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        // Read input content
                        string? contentJson = null;
                        if (context.Request.ContentLength64 > 0)
                        {
                            using StreamReader input = new(context.Request.InputStream);
                            contentJson = input.ReadToEnd();
                        }

                        // Fire event for processing
                        ProcessingRequestEventArgs args = new(path, contentJson);
                        ProcessingRequest?.Invoke(this, args);

                        // Set response parameters
                        context.Response.StatusCode = (int)args.ResponseStatusCode;
                        context.Response.StatusDescription = args.ResponseStatusDescription;

                        // Write response content
                        if (args.ResponseContent != null)
                        {
                            context.Response.ContentType = ContentTypes.CONTENT_TYPE_JSON;
                            using StreamWriter output = new(context.Response.OutputStream);
                            output.Write(args.ResponseContent);
                        }

                        // Close response to close the request
                        context.Response.OutputStream.Close();
                    }
                }
            }
        }
    }
}
