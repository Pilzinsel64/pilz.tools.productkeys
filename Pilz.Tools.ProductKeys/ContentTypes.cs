﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys
{
    internal static class ContentTypes
    {
        public const string CONTENT_TYPE_JSON = "application/json";
    }
}
