﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Requests
{
    internal class NewKeyValidationResponse
    {
        public long? KeyIdentification { get; set; }
    }
}
