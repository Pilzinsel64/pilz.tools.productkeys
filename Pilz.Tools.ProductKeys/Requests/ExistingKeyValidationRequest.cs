﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Requests
{
    internal class ExistingKeyValidationRequest
    {
        public string ClientSecret { get; set; }
        public long KeyIdentification { get; set; }

        [JsonConstructor]
        public ExistingKeyValidationRequest(string clientSecret, long keyIdentification)
        {
            ClientSecret = clientSecret;
            KeyIdentification = keyIdentification;
        }
    }
}
