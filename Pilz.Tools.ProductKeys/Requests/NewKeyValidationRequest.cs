﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Requests
{
    internal class NewKeyValidationRequest
    {
        public string ProductKey { get; set; }
        public string ClientSecret { get; set; }

        [JsonConstructor]
        public NewKeyValidationRequest(string productKey, string clientSecret)
        {
            ProductKey = productKey;
            ClientSecret = clientSecret;
        }
    }
}
