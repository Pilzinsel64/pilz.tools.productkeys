﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys
{
    internal class UrlRequestPaths
    {
        public const string VALIDATE_NEW_KEY = "/validate";
        public const string VALIDATE_EXISTNG_KEY = "/check";
    }
}
