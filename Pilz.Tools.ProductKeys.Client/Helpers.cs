﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Tools.ProductKeys.Client
{
    internal static class Helpers
    {
        private static string? clientSecret = null;

        public static string ClientSecret => clientSecret ??= CalculateClientSecret();

        private static string CalculateClientSecret()
        {
            MD5 md5 = MD5.Create();
            string clientSecretRaw = Cryptography.Helpers.CalculateClientSecret();
            string clientSecret = BitConverter.ToString(md5.ComputeHash(Encoding.Default.GetBytes(clientSecretRaw))).Replace("-", string.Empty);
            md5.Dispose();
            return clientSecret;
        }
    }
}
