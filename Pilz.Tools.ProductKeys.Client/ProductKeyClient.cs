﻿using Newtonsoft.Json;
using Pilz.Tools.ProductKeys.Requests;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace Pilz.Tools.ProductKeys.Client
{
    public class ProductKeyClient : IDisposable
    {
        private readonly HttpClient httpClient = new();

        public string ServerUrl { get; init; }

        public ProductKeyClient(string serverUrl)
        {
            ServerUrl = serverUrl;
        }

        ~ProductKeyClient()
        {
            Dispose();
        }

        public long? ValidateProductKey(string productKey)
        {
            long? keyIdentification = null;

            if (!string.IsNullOrWhiteSpace(productKey))
            {
                NewKeyValidationRequest request = new(productKey, Helpers.ClientSecret);
                HttpResponseMessage httpResponse = MakeRequest(UrlRequestPaths.VALIDATE_NEW_KEY, request);

                if (httpResponse.IsSuccessStatusCode)
                {
                    string strResponse = httpResponse.Content.ReadAsStringAsync().Result;
                    NewKeyValidationResponse? response = JsonConvert.DeserializeObject<NewKeyValidationResponse>(strResponse);
                    keyIdentification = response?.KeyIdentification;
                }
            }

            return keyIdentification;
        }

        public bool ValidateProductKey(long keyIdentification)
        {
            bool isValid = false;

            ExistingKeyValidationRequest request = new(Helpers.ClientSecret, keyIdentification);
            HttpResponseMessage httpResponse = MakeRequest(UrlRequestPaths.VALIDATE_EXISTNG_KEY, request);

            if (httpResponse.IsSuccessStatusCode)
                isValid = true;

            return isValid;
        }

        private HttpResponseMessage MakeRequest(string requestPath, object? content)
        {
            HttpRequestMessage request = new()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(ServerUrl + requestPath),
                Headers =
                {
                    { "Accept", ContentTypes.CONTENT_TYPE_JSON },
                },
                Content = new StringContent(JsonConvert.SerializeObject(content), null, ContentTypes.CONTENT_TYPE_JSON)
            };

            return httpClient.Send(request);
        }

        public void Dispose()
        {
            httpClient?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}