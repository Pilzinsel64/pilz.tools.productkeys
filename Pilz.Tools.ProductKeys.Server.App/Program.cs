﻿using Mono.Options;
using Pilz.Networking.CloudProviders.Nextcloud;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Model;
using Pilz.Tools.ProductKeys.Server.Connectors.NextcloudTables;

namespace Pilz.Tools.ProductKeys.Server.App
{
    internal class Program
    {
        private const string SERVER_URL_TEMPLATE = "http://localhost:{0}/";

        private static readonly NextcloudLogin ncLogin = new();
        private static readonly NextcloudClient ncClient = new();
        private static readonly NextcloudTablesConnectorSettings connectorSettings = new();
        private static readonly NextcloudTablesConnector connector = new(ncClient, connectorSettings);
        private static ProductKeyServer? server = null;
        private static string? serverUrl = null;
        private static bool shouldShowHelp = false;
        private static bool shouldListColumns = false;
        private static readonly OptionSet startupOptions = new()
        {
            {
                "-p|port=",
                "The {PORT} to listen to.",
                (uint port) => serverUrl = string.Format(SERVER_URL_TEMPLATE, port)
            },
            {
                "s|server=",
                "The {SERVER} url of the Nextcloud instance.",
                loginName => ncLogin.Server = loginName
            },
            {
                "l|login=",
                "The Nextcloud {LOGIN} name to use.\nNote: The user must have read+write permissions for the table!",
                loginName => ncLogin.LoginName = loginName
            },
            {
                "p|password=",
                "The (app){PASSWORD} for the Nextcloud login.",
                appPassword => ncLogin.AppPassword = appPassword
            },
            {
                "h|help",
                "Shows this help message. No other action will be executed.",
                _ => shouldShowHelp = true
            },
            {
                "tableid=",
                "The id of the table.",
                (long tableid) => connectorSettings.TableId = tableid
            },
            {
                "columnid_key=",
                "The id of the column that contains the product key.",
                (long columnid_key) => connectorSettings.ColumnIdKey = columnid_key
            },
            {
                "columnid_state=",
                "The id of the column that contains the current state.",
                (long columnid_state) => connectorSettings.ColumnIdState = columnid_state
            },
            {
                "columnid_secret=",
                "The id of the column that contains the client secret.",
                (long columnid_secret) => connectorSettings.ColumnIdClientSecret = columnid_secret
            },
            {
                "columnid_lastaccess=",
                "The id of the column that contains the last access time.",
                (long columnid_lastaccess) => connectorSettings.ColumnIdLastAccess = columnid_lastaccess
            },
            {
                "listcolumns",
                "Lists all columns of the table with their id and their name. No other action will be executed.",
                _ => shouldListColumns = true
            }
        };

        static void Main(string[] args)
        {
            // Parse start arguments
            try
            {
                startupOptions.Parse(args);
            }
            catch(OptionException ex)
            {
                Console.Write("Argument Error: ");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try '--help' for more information.");
                return;
            }

            // Sanity checks for required arguments
            if (serverUrl == null)
            {
                Console.WriteLine("Some required arguments were not defined.");
                Console.WriteLine("Try '--help' for more information.");
                return;
            }

            // Draw help
            if (shouldShowHelp)
            {
                startupOptions.WriteOptionDescriptions(Console.Out);
                return;
            }

            // Start the server
            if (ncClient.Login(ncLogin) is UserInfo userInfo)
            {
                if (shouldListColumns)
                {
                    Columns? columns = ncClient.GetClient<TablesClient>().GetColumns(connectorSettings.TableId);

                    if (columns != null)
                    {
                        foreach (Column column in columns)
                            Console.WriteLine($"{column.ColumnId}\t{column.Title}");
                    }
                    else
                        Console.WriteLine($"Failed seraching for columns in tableid={connectorSettings.TableId}.");
                }
                else
                {
                    server = new(serverUrl, connector, Console.Out);
                    server.StartServer();

                    Console.WriteLine("Server startet and listen to: " + serverUrl);
                    Console.WriteLine("Write \"exit\" and press enter to stop the server and close this program.");
                    Console.ReadLine();
                }
            }
            else
                Console.WriteLine("Login to Nextcloud not possible!");

            // Finalize
            server?.StopServer();
            ncClient.Logout();
            server?.Dispose();

            Console.WriteLine("Server stopped. Exiting program...");
        }
    }
}